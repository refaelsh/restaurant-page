" nnoremap <F4> :!rm -r build/*<CR>
" nnoremap <F5> :wa <bar> :set makeprg=cd\ build\ &&\ cmake\ -DCMAKE_BUILD_TYPE=debug\ -DCMAKE_EXPORT_COMPILE_COMMANDS=1\ ../view\ &&\ cmake\ --build\ . <bar> :compiler gcc <bar> :make <CR>
" nnoremap <F6> :wa <bar> :compiler cargo <bar> :make build <CR>
" nnoremap <F7> :wa <bar> :compiler cargo <bar> :make test<CR>
" nnoremap <F10> :silent !./build/giter <CR> :redraw! <CR>
nnoremap <F7> :wa <bar> :compiler jest <bar> :make<CR>
:set backupcopy=yes
