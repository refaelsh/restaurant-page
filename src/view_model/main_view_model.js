import * as ko from "knockout";

import homeTabHTML from "../view/home_tab/home_tab.html";
import "../view/home_tab/home_tab.css";

import menuTabHTML from "../view/menu_tab/menu_tab.html";
import "../view/menu_tab/menu_tab.css";

import contactTabHTML from "../view/contact_tab/contact_tab.html";
import "../view/contact_tab/contact_tab.css";

export class MainViewModel {
  constructor(menuViewModel) {
    this.menuViewModel = menuViewModel;

    this.tabContents = ko.observable(homeTabHTML);

    this.isHomeTabSelected = ko.observable(true);
    this.isMenuTabSelected = ko.observable(false);
    this.isContactTabSelected = ko.observable(false);
    this.menuItemTitle = ko.observable(`Some title`);
  }

  homeTabClick() {
    this.tabContents(homeTabHTML);

    this.isHomeTabSelected(true);
    this.isMenuTabSelected(false);
    this.isContactTabSelected(false);
  };

  menuTabClick() {
    this.tabContents(menuTabHTML);

    const bla = document.getElementById(`div-menu-wrapper`);
    ko.applyBindings(this.menuViewModel, bla);

    this.isHomeTabSelected(false);
    this.isMenuTabSelected(true);
    this.isContactTabSelected(false);
  };

  contactTabClick() {
    this.tabContents(contactTabHTML);

    this.isHomeTabSelected(false);
    this.isMenuTabSelected(false);
    this.isContactTabSelected(true);
  };
}
