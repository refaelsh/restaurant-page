import * as ko from "knockout";

import imgSalsiccia from "../view/menu_tab/salsiccia.png";
import imgPomodoro from "../view/menu_tab/pomodoro.png";
import imgPepe from "../view/menu_tab/pepe.png";
import imgGamberi from "../view/menu_tab/gamberi.png";
import imgDisgustoso from "../view/menu_tab/disgustoso.png";
import imgCrema from "../view/menu_tab/crema.png";
import imgColorato from "../view/menu_tab/colorato.png";
import imgCarne from "../view/menu_tab/carne.png";

export class MenuViewModel {
  constructor() {
    this.titles = ko.observableArray([
      { title: `Salsiccia`, image: imgSalsiccia },
      { title: `Pomodoro`, image: imgPomodoro },
      { title: `Pepe`, image: imgPepe },
      { title: `Gamberi`, image: imgGamberi },
      { title: `Disgustoso`, image: imgDisgustoso },
      { title: `Crema`, image: imgCrema },
      { title: `Colorato`, image: imgColorato },
      { title: `Carne`, image: imgCarne }]);
  }
}
