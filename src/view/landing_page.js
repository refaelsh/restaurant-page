import * as ko from "knockout";
import "./landing_page.css";

import { MenuViewModel } from "../view_model/menu_view_model.js";
import { MainViewModel } from "../view_model/main_view_model.js";

const menuViewModel = new MenuViewModel();
const mainViewModel = new MainViewModel(menuViewModel);
ko.applyBindings(mainViewModel);
